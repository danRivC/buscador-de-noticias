import React, {Fragment, useState, useEffect} from 'react';
import Header from './components/Header';
import Formulario from './components/Formulario';
import ListadoNoticias from './components/ListadoNoticias'
function App() {
  const [categoria, guardarCategoria] = useState('');
  const [noticias, guardarNoticias] = useState([]);
  const ListadoComponent = (noticias)?<ListadoNoticias 
  noticias={noticias}
/>: null
  useEffect(()=>{
    const consultarApi= async() => {
      const apiKey = '738a71e6b7cc41cebcdcee50373618c8'
      const url =`https://newsapi.org/v2/top-headlines?country=mx&category=${categoria}&apiKey=${apiKey}`
      const respuesta = await fetch(url)
      const noticias =await respuesta.json();
      guardarNoticias(noticias.articles)
    }
    consultarApi();
    console.log(noticias)
  }, [categoria]);
  return (
    <Fragment>
      <Header titulo="Buscador de Noticias" />
      <div className="container white">
        <Formulario 
          guardarCategoria={guardarCategoria}
        />
        {ListadoComponent}
      </div>
    </Fragment>
  );
}

export default App;
